// !DO NOT EDIT THIS FILE!
// This source file is generated by Oracle tools
// Contents may be subject to change
// For reporting problems, use the following
// Version = Oracle WebServices (10.1.3.5.0, build 090727.2000.36696)

package com.fmcna.osm.integration;


public class SetOrdServiceDateInShiftElement implements java.io.Serializable {
    protected com.fmcna.osm.integration.PatshiftordersRecUser[] pPatshiftorders;
    protected java.util.Calendar pServiceDate;
    protected java.lang.String pSoarianUserid;
    
    public SetOrdServiceDateInShiftElement() {
    }
    
    public com.fmcna.osm.integration.PatshiftordersRecUser[] getPPatshiftorders() {
        return pPatshiftorders;
    }
    
    public void setPPatshiftorders(com.fmcna.osm.integration.PatshiftordersRecUser[] pPatshiftorders) {
        this.pPatshiftorders = pPatshiftorders;
    }
    
    public java.util.Calendar getPServiceDate() {
        return pServiceDate;
    }
    
    public void setPServiceDate(java.util.Calendar pServiceDate) {
        this.pServiceDate = pServiceDate;
    }
    
    public java.lang.String getPSoarianUserid() {
        return pSoarianUserid;
    }
    
    public void setPSoarianUserid(java.lang.String pSoarianUserid) {
        this.pSoarianUserid = pSoarianUserid;
    }
}
