// !DO NOT EDIT THIS FILE!
// This source file is generated by Oracle tools
// Contents may be subject to change
// For reporting problems, use the following
// Version = Oracle WebServices (10.1.3.5.0, build 090727.2000.36696)

package com.fmcna.osm.integration;


public class GetPatientsInShiftElement implements java.io.Serializable {
    protected java.math.BigDecimal pClinicId;
    protected java.util.Calendar pServiceDate;
    protected java.lang.String pShiftNum;
    
    public GetPatientsInShiftElement() {
    }
    
    public java.math.BigDecimal getPClinicId() {
        return pClinicId;
    }
    
    public void setPClinicId(java.math.BigDecimal pClinicId) {
        this.pClinicId = pClinicId;
    }
    
    public java.util.Calendar getPServiceDate() {
        return pServiceDate;
    }
    
    public void setPServiceDate(java.util.Calendar pServiceDate) {
        this.pServiceDate = pServiceDate;
    }
    
    public java.lang.String getPShiftNum() {
        return pShiftNum;
    }
    
    public void setPShiftNum(java.lang.String pShiftNum) {
        this.pShiftNum = pShiftNum;
    }
}
