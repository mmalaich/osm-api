// !DO NOT EDIT THIS FILE!
// This source file is generated by Oracle tools
// Contents may be subject to change
// For reporting problems, use the following
// Version = Oracle WebServices (10.1.3.5.0, build 090727.2000.36696)

package com.fmcna.osm.integration.runtime;

import oracle.j2ee.ws.common.encoding.*;
import oracle.j2ee.ws.common.encoding.literal.*;
import oracle.j2ee.ws.common.encoding.literal.DetailFragmentDeserializer;
import oracle.j2ee.ws.common.encoding.simpletype.*;
import oracle.j2ee.ws.common.soap.SOAPEncodingConstants;
import oracle.j2ee.ws.common.soap.SOAPEnvelopeConstants;
import oracle.j2ee.ws.common.soap.SOAPVersion;
import oracle.j2ee.ws.common.streaming.*;
import oracle.j2ee.ws.common.wsdl.document.schema.SchemaConstants;
import oracle.j2ee.ws.common.util.xml.UUID;
import javax.xml.namespace.QName;
import java.util.List;
import java.util.ArrayList;
import java.util.StringTokenizer;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.AttachmentPart;

public class LabOrderRecUser_LiteralSerializerproxy extends LiteralObjectSerializerBase implements Initializable {
    private static final QName ns1_labOrdTab_QNAME = new QName("http://integration.osm.fmcna.com/", "labOrdTab");
    private static final QName ns1_LabTab_TYPE_QNAME = new QName("http://integration.osm.fmcna.com/", "LabTab");
    private CombinedSerializer myns1_LabTab__LabTab_LiteralSerializerproxy;
    private static final QName ns1_statusMsg_QNAME = new QName("http://integration.osm.fmcna.com/", "statusMsg");
    private static final QName ns2_string_TYPE_QNAME = SchemaConstants.QNAME_TYPE_STRING;
    private CombinedSerializer myns2_string__java_lang_String_String_Serializer;
    private static final QName ns1_statusCd_QNAME = new QName("http://integration.osm.fmcna.com/", "statusCd");
    
    public LabOrderRecUser_LiteralSerializerproxy(QName type) {
        this(type,  false);
    }
    
    public LabOrderRecUser_LiteralSerializerproxy(QName type, boolean encodeType) {
        super(type, true, encodeType);
        setSOAPVersion(SOAPVersion.SOAP_11);
    }
    
    public void initialize(InternalTypeMappingRegistry registry) throws Exception {
        myns1_LabTab__LabTab_LiteralSerializerproxy = (CombinedSerializer)registry.getSerializer("", com.fmcna.osm.integration.LabTab.class, ns1_LabTab_TYPE_QNAME);
        myns2_string__java_lang_String_String_Serializer = (CombinedSerializer)registry.getSerializer("", java.lang.String.class, ns2_string_TYPE_QNAME);
    }
    
    public java.lang.Object doDeserialize(XMLReader reader,
        SOAPDeserializationContext context) throws Exception {
        com.fmcna.osm.integration.LabOrderRecUser instance = new com.fmcna.osm.integration.LabOrderRecUser();
        java.lang.Object member=null;
        QName elementName;
        List values;
        java.lang.Object value;
        
        reader.nextElementContent();
        java.util.HashSet requiredElements = new java.util.HashSet();
        requiredElements.add("labOrdTab");
        requiredElements.add("statusMsg");
        requiredElements.add("statusCd");
        for (int memberIndex = 0; memberIndex <3; memberIndex++) {
            elementName = reader.getName();
            if ((reader.getState() == XMLReader.START) && (matchQName(elementName, ns1_labOrdTab_QNAME))) {
                myns1_LabTab__LabTab_LiteralSerializerproxy.setNullable( false );
                context.setNillable( true );
                member = myns1_LabTab__LabTab_LiteralSerializerproxy.deserialize(ns1_labOrdTab_QNAME, reader, context);
                requiredElements.remove("labOrdTab");
                instance.setLabOrdTab((com.fmcna.osm.integration.LabTab)member);
                context.setXmlFragmentWrapperName( null );
                reader.nextElementContent();
            }
            if ((reader.getState() == XMLReader.START) && (matchQName(elementName, ns1_statusMsg_QNAME))) {
                myns2_string__java_lang_String_String_Serializer.setNullable( false );
                context.setNillable( true );
                member = myns2_string__java_lang_String_String_Serializer.deserialize(ns1_statusMsg_QNAME, reader, context);
                requiredElements.remove("statusMsg");
                instance.setStatusMsg((java.lang.String)member);
                context.setXmlFragmentWrapperName( null );
                reader.nextElementContent();
            }
            if ((reader.getState() == XMLReader.START) && (matchQName(elementName, ns1_statusCd_QNAME))) {
                myns2_string__java_lang_String_String_Serializer.setNullable( false );
                context.setNillable( true );
                member = myns2_string__java_lang_String_String_Serializer.deserialize(ns1_statusCd_QNAME, reader, context);
                requiredElements.remove("statusCd");
                instance.setStatusCd((java.lang.String)member);
                context.setXmlFragmentWrapperName( null );
                reader.nextElementContent();
            }
        }
        if (!requiredElements.isEmpty()) {
            throw new DeserializationException( "literal.expectedElementName" , requiredElements.iterator().next().toString(), DeserializationException.FAULT_CODE_CLIENT );
        }
        
        if( reader.getState() != XMLReader.END)
        {
            reader.skipElement();
        }
        XMLReaderUtil.verifyReaderState(reader, XMLReader.END);
        return (java.lang.Object)instance;
    }
    
    public void doSerializeAttributes(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws Exception {
        com.fmcna.osm.integration.LabOrderRecUser instance = (com.fmcna.osm.integration.LabOrderRecUser)obj;
        
    }
    public void doSerializeAnyAttributes(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws Exception {
        com.fmcna.osm.integration.LabOrderRecUser instance = (com.fmcna.osm.integration.LabOrderRecUser)obj;
        
    }
    public void doSerialize(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws Exception {
        com.fmcna.osm.integration.LabOrderRecUser instance = (com.fmcna.osm.integration.LabOrderRecUser)obj;
        
        myns1_LabTab__LabTab_LiteralSerializerproxy.setNullable( false );
        context.setNillable( true );
        myns1_LabTab__LabTab_LiteralSerializerproxy.serialize(instance.getLabOrdTab(), ns1_labOrdTab_QNAME, null, writer, context);
        myns2_string__java_lang_String_String_Serializer.setNullable( false );
        context.setNillable( true );
        myns2_string__java_lang_String_String_Serializer.serialize(instance.getStatusMsg(), ns1_statusMsg_QNAME, null, writer, context);
        myns2_string__java_lang_String_String_Serializer.setNullable( false );
        context.setNillable( true );
        myns2_string__java_lang_String_String_Serializer.serialize(instance.getStatusCd(), ns1_statusCd_QNAME, null, writer, context);
    }
}
