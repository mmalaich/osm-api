// !DO NOT EDIT THIS FILE!
// This source file is generated by Oracle tools
// Contents may be subject to change
// For reporting problems, use the following
// Version = Oracle WebServices (10.1.3.5.0, build 090727.2000.36696)

package com.fmcna.osm.integration.runtime;

import oracle.j2ee.ws.common.encoding.*;
import oracle.j2ee.ws.common.encoding.literal.*;
import oracle.j2ee.ws.common.encoding.literal.DetailFragmentDeserializer;
import oracle.j2ee.ws.common.encoding.simpletype.*;
import oracle.j2ee.ws.common.soap.SOAPEncodingConstants;
import oracle.j2ee.ws.common.soap.SOAPEnvelopeConstants;
import oracle.j2ee.ws.common.soap.SOAPVersion;
import oracle.j2ee.ws.common.streaming.*;
import oracle.j2ee.ws.common.wsdl.document.schema.SchemaConstants;
import oracle.j2ee.ws.common.util.xml.UUID;
import javax.xml.namespace.QName;
import java.util.List;
import java.util.ArrayList;
import java.util.StringTokenizer;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.AttachmentPart;

public class GetPatientsInShiftResponseElement_LiteralSerializerproxy extends LiteralObjectSerializerBase implements Initializable {
    private static final QName ns1_result_QNAME = new QName("http://integration.osm.fmcna.com/", "result");
    private static final QName ns1_PatshiftlistRecUser_TYPE_QNAME = new QName("http://integration.osm.fmcna.com/", "PatshiftlistRecUser");
    private CombinedSerializer myns1_PatshiftlistRecUser__PatshiftlistRecUser_LiteralSerializerproxy;
    
    public GetPatientsInShiftResponseElement_LiteralSerializerproxy(QName type) {
        this(type,  false);
    }
    
    public GetPatientsInShiftResponseElement_LiteralSerializerproxy(QName type, boolean encodeType) {
        super(type, true, encodeType);
        setSOAPVersion(SOAPVersion.SOAP_11);
    }
    
    public void initialize(InternalTypeMappingRegistry registry) throws Exception {
        myns1_PatshiftlistRecUser__PatshiftlistRecUser_LiteralSerializerproxy = (CombinedSerializer)registry.getSerializer("", com.fmcna.osm.integration.PatshiftlistRecUser.class, ns1_PatshiftlistRecUser_TYPE_QNAME);
    }
    
    public java.lang.Object doDeserialize(XMLReader reader,
        SOAPDeserializationContext context) throws Exception {
        com.fmcna.osm.integration.GetPatientsInShiftResponseElement instance = new com.fmcna.osm.integration.GetPatientsInShiftResponseElement();
        java.lang.Object member=null;
        QName elementName;
        List values;
        java.lang.Object value;
        
        reader.nextElementContent();
        java.util.HashSet requiredElements = new java.util.HashSet();
        elementName = reader.getName();
        if ((reader.getState() == XMLReader.START) && (matchQName(elementName, ns1_result_QNAME))) {
            values = new ArrayList();
            for(;;) {
                elementName = reader.getName();
                if ((reader.getState() == XMLReader.START) && (matchQName(elementName, ns1_result_QNAME))) {
                    myns1_PatshiftlistRecUser__PatshiftlistRecUser_LiteralSerializerproxy.setNullable( false );
                    context.setNillable( true );
                    value = myns1_PatshiftlistRecUser__PatshiftlistRecUser_LiteralSerializerproxy.deserialize(ns1_result_QNAME, reader, context);
                    requiredElements.remove("result");
                    values.add(value);
                    reader.nextElementContent();
                } else {
                    break;
                }
            }
            member = new com.fmcna.osm.integration.PatshiftlistRecUser[values.size()];
            member = values.toArray((java.lang.Object[]) member);
            instance.setResult((com.fmcna.osm.integration.PatshiftlistRecUser[])member);
        }
        else {
            if (instance.getResult() == null)
            instance.setResult(new com.fmcna.osm.integration.PatshiftlistRecUser[0]);
        }
        if (!requiredElements.isEmpty()) {
            throw new DeserializationException( "literal.expectedElementName" , requiredElements.iterator().next().toString(), DeserializationException.FAULT_CODE_CLIENT );
        }
        
        if( reader.getState() != XMLReader.END)
        {
            reader.skipElement();
        }
        XMLReaderUtil.verifyReaderState(reader, XMLReader.END);
        return (java.lang.Object)instance;
    }
    
    public void doSerializeAttributes(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws Exception {
        com.fmcna.osm.integration.GetPatientsInShiftResponseElement instance = (com.fmcna.osm.integration.GetPatientsInShiftResponseElement)obj;
        
    }
    public void doSerializeAnyAttributes(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws Exception {
        com.fmcna.osm.integration.GetPatientsInShiftResponseElement instance = (com.fmcna.osm.integration.GetPatientsInShiftResponseElement)obj;
        
    }
    public void doSerialize(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws Exception {
        com.fmcna.osm.integration.GetPatientsInShiftResponseElement instance = (com.fmcna.osm.integration.GetPatientsInShiftResponseElement)obj;
        
        if (instance.getResult() != null) {
            for (int i = 0; i < instance.getResult().length; ++i) {
                myns1_PatshiftlistRecUser__PatshiftlistRecUser_LiteralSerializerproxy.setNullable( false );
                context.setNillable( true );
                myns1_PatshiftlistRecUser__PatshiftlistRecUser_LiteralSerializerproxy.serialize(instance.getResult()[i], ns1_result_QNAME, null, writer, context);
            }
        }
    }
}
